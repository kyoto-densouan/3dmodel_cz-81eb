# README #

1/3スケールのシャープX1用拡張ボックスCZ-81EB風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-81eb/raw/f29b966c9e07e3a96188e8509e05f7687cda0c8d/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-81eb/raw/f29b966c9e07e3a96188e8509e05f7687cda0c8d/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-81eb/raw/f29b966c9e07e3a96188e8509e05f7687cda0c8d/ExampleImage.png)
